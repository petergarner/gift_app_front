export class AuthController {

  constructor($auth, $state) {
    'ngInject'

    this.$auth = $auth
    this.$state = $state
    this.NETWORK_ERROR = 'cannot contact server'
    this.clearStorage()
  }

  register() {
    var vm = this
    self.$state = this.$state
    this.$auth.signup(this.user)
      .then((token) => {
        if (!token.data) {
          alert(token.statusText);
          vm.login_error = token.statusText
          return
        }
        vm.$auth.setToken(token)
        self.$state.go('account')
      })
      .catch(err => {
        if (err.status === -1) {
          alert(this.NETWORK_ERROR);
        }
      })
  }

  login() {
    var vm = this
    self.$state = this.$state
    this.$auth.login(vm.login.user)
      .then((token) => {
        if (!token.data) {
          alert(token.statusText);
          vm.login.user = ''
          vm.login.pwd = ''
          vm.login_error = token.statusText
          return
        }
        vm.$auth.setToken(token)
        self.$state.go('account')
      })
      .catch(err => {
        if (err.status === -1) {
          alert(this.NETWORK_ERROR);
        }
      })
  }

  clearStorage() {
    localStorage.removeItem('friend data')
    localStorage.removeItem('satellizer_token')
    localStorage.removeItem('debug')
  }
}
