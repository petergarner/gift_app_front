export class AccountController {

  constructor($http, $auth, $state, API_URL, $timeout) {
    'ngInject'

    this.$http = $http
    this.$auth = $auth
    this.$state = $state
    this.$timeout = $timeout
    this.API_URL = API_URL
    this.NETWORK_ERROR = 'cannot contact server'
    this.getAccount()
    this.getRequests()
    this.getContacts()
  }

  getAccount() {
    var vm = this;
    var token = vm.$auth.getToken()
    this.$http.get(this.API_URL + '/api/accountwallet', { params: { token: token } }).then((result) => {
        vm.pubkey = result.data.pubaddr
        vm.name = result.data.name
        vm.btc_unconf = result.data.btc_unconf
      })
      .catch(err => {
        if (err.status === -1) {
          alert(this.NETWORK_ERROR);
        }
      })
  }

  postRequest() {
    var vm = this
    this.$http.post(this.API_URL + '/api/requests', { item: this.item }).then((result) => {
        alert(result.statusText);
        function reloadPage() {
          this.$state.reload('account')
        }
        this.$timeout(reloadPage, 1000)
        vm.item = ''
      })
      .catch(err => {
        if (err.status === -1) {
          alert(this.NETWORK_ERROR);
        }
      })
  }

  getContacts() {
    var vm = this
    var token = vm.$auth.getToken()
    this.$http.get(this.API_URL + '/api/accountcontacts', { params: { token: token } }).then((result) => {
        vm.contacts = result.data
      })
      .catch(err => {
        if (err.status === -1) {
          alert(this.NETWORK_ERROR);
        }
      })
  }

  getRequests() {
    var vm = this
    var token = vm.$auth.getToken()
    this.$http.get(this.API_URL + '/api/requests', { params: { token: token } }).then((result) => {
        result.data.forEach(() => {
          if (result.data[0].order_complete) {
            result.data[0].order_status = 'Item Ordered!'
          }
        })
        vm.requests = result.data
      })
      .catch(err => {
        if (err.status === -1) {
          alert(this.NETWORK_ERROR);
        }
      })
  }

  buyRequest(item) {
    this.$http.post(this.API_URL + '/api/buyitem', { item: item, name: this.name }).then((result) => {
        alert(result.statusText)

        function reloadPage() {
          this.$state.reload('account');
        }
        this.$timeout(reloadPage, 1000)
      })
      .catch(err => {
        if (err.status === -1) {
          alert(this.NETWORK_ERROR);
        }
      })
  }
}
