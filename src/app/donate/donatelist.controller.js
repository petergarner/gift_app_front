export class DonatelistController {
  constructor($http, $auth, $stateParams, API_URL) {
    'ngInject'

    this.$http = $http
    this.$auth = $auth
    this.$stateParams = $stateParams
    this.API_URL = API_URL
    this.NETWORK_ERROR = 'cannot contact server'
    this.getRequests()
  }

  getRequests() {
    var vm = this
    var token = vm.$auth.getToken()
    var friend = this.$stateParams.userID
    vm.friend = friend
    this.$http.get(this.API_URL + '/api/requests', { params: { token: token, friend: friend } })
      .then((result) => {
        result.data.forEach(() => {
          if (result.data[0].order_complete) {
            result.data[0].order_status = 'Item Ordered!'
          }
        })
        vm.requests = result.data
      })
      .catch(err => {
        if (err.status === -1) {
          alert(this.NETWORK_ERROR);
        }
      })
  }
  clearStorage() {
    localStorage.removeItem('friend data')
  }

}
