export class NavbarController {
  constructor($auth, $state) {
    'ngInject';

    this.$auth = $auth;
    this.$state = $state;
    this.isAuthenticated = $auth.isAuthenticated;
  }

  logout() {
    this.$auth.logout();
    localStorage.removeItem('friend data')
    self.$state.go('auth')

  }

  homePage() {
    self.$state.go('account');
  }

  shopping() {
    self.$state.go('shopping');
  }

    clearStorage() {
    localStorage.removeItem('friend data')
    localStorage.removeItem('token')
  }
}
