export class ShoppingController {

  constructor($http, $auth, API_URL) {
    'ngInject'

    this.$http = $http
    this.$auth = $auth
    this.API_URL = API_URL
    this.NETWORK_ERROR = 'cannot contact server'
    this.getItemValues()
  }

  getItemValues() {
    var vm = this
    var token = vm.$auth.getToken()
    this.$http.get(this.API_URL + '/api/items', { params: { token: token } }).then((result) => {
        vm.items = result.data
      })
      .catch(err => {
        if (err.status === -1) {
          alert(this.NETWORK_ERROR);
        }
      })
  }
}
