export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject'
  $stateProvider
    .state('auth', {
      url: '/auth',
      templateUrl: 'app/auth/auth.html',
      controller: 'AuthController',
      controllerAs: 'auth'
    })
    .state('account', {
      url: '/account',
      templateUrl: 'app/account/account.html',
      controller: 'AccountController',
      controllerAs: 'account'
    })
    .state('donatelist', {
      url: '/donatelist/:userID',
      templateUrl: 'app/donate/donatelist.html',
      controller: 'DonatelistController',
      controllerAs: 'donatelist'
    })
    .state('donate', {
        url: "/donate",
        params: {
            friend: null,
            item: null,
            cost: null,
            unconf: null,
            item_id: null
        },
      templateUrl: 'app/donate/donate.html',
      controller: 'DonateController',
      controllerAs: 'donate'
    })
    .state('shopping', {
      url: '/shopping',
      templateUrl: 'app/shopping/shopping.html',
      controller: 'ShoppingController',
      controllerAs: 'shopping'
    });
  $urlRouterProvider.otherwise('/auth')
}
