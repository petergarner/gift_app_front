// /* global malarkey:false, moment:false */

import { config } from './index.config'
import { routerConfig } from './index.route'
import { runBlock } from './index.run'
import { AuthController } from './auth/auth.controller'
import { NavbarController } from './components/navbar/navbar.controller'
import { AccountController } from './account/account.controller'
import { DonatelistController } from './donate/donatelist.controller'
import { DonateController } from './donate/donate.controller'
import { ShoppingController } from './shopping/shopping.controller'
import { CompareToDirective } from './directives/compareTo.directive'
import { NavbarDirective } from '../app/components/navbar/navbar.directive'

angular.module('GiftCoin', ['ui.router', 'ui.bootstrap',  'satellizer'])
  .constant('API_URL', 'http://localhost:5000')
  .constant('moment', moment)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .controller('AuthController', AuthController)
.controller('NavbarController', NavbarController)
  .directive('acmeNavbar', NavbarDirective)
.controller('AccountController', AccountController)
.controller('DonatelistController', DonatelistController)
.controller('DonateController', DonateController)
.controller('ShoppingController', ShoppingController)
  .directive('compareTo', CompareToDirective)
